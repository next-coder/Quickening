//
//  CanlendarHomeViewController.swift
//  Quickening
//
//  Created by xn011644 on 5/18/16.
//  Copyright © 2016 Jasper. All rights reserved.
//

import UIKit

class CalendarHomeViewController: BaseCollectionViewController, UICollectionViewDelegateFlowLayout {

    let calendarHomeCollectionCellIdentifier = "calendarHomeCollectionCellIdentifier"
    let calendarHomeMonthHeaderIdentifier = "calendarHomeMonthHeaderIdentifier"

    var quickeningButton: UIButton?

    init() {

        let collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.itemSize = CGSize(width: UIScreen.mainScreen().bounds.size.width / 7.0,
                                           height: 80.0)
        collectionLayout.headerReferenceSize = CGSize(width: UIScreen.mainScreen().bounds.size.width,
                                                      height: 40)
        collectionLayout.minimumInteritemSpacing = 0
        super.init(collectionViewLayout: collectionLayout)
    }

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "宝贝胎动"

        self.collectionView?.backgroundColor = UIColor.whiteColor()
        self.collectionView?.registerClass(YearCalendarDayCell.self,
                                           forCellWithReuseIdentifier: calendarHomeCollectionCellIdentifier)
        self.collectionView?.registerClass(CalendarMonthHeaderView.self,
                                           forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                           withReuseIdentifier: calendarHomeMonthHeaderIdentifier)

        self.addQuickeningButton()
    }

    func addQuickeningButton() {

        let quickeningButton = UIButton(type: .Custom)
        quickeningButton.backgroundColor = UIColor.blueColor()
        quickeningButton.setTitle("新胎动", forState: .Normal)
        quickeningButton.titleLabel?.font = UIFont.systemFontOfSize(13)
        quickeningButton.addTarget(self,
                                   action: #selector(quickening(_:)),
                                   forControlEvents: .TouchUpInside)
        self.view.addSubview(quickeningButton)
        self.quickeningButton = quickeningButton
        self.quickeningButton?.snp_makeConstraints(closure: { (make) in

            make.bottom.equalTo(self.view.snp_bottom).offset(-20)
            make.centerX.equalTo(self.view)
            make.width.equalTo(80)
            make.height.equalTo(80)
        })
    }

    // UICollectionViewDataSource
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {

        return 1
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 60
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell: YearCalendarDayCell = collectionView.dequeueReusableCellWithReuseIdentifier(calendarHomeCollectionCellIdentifier, forIndexPath: indexPath) as! YearCalendarDayCell
        cell.dayLabel?.text = String(indexPath.row)
        cell.contentButton?.setTitle("99", forState: .Disabled)

        return cell
    }

    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {

        if kind == UICollectionElementKindSectionHeader {

            let header: CalendarMonthHeaderView = collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                                                                                                        withReuseIdentifier: calendarHomeMonthHeaderIdentifier,
                                                                                                        forIndexPath: indexPath) as! CalendarMonthHeaderView
            header.monthLabel?.text = String(NSDateComponents().month)
            header.updateMonthLabelToIndex(1)
            return header
        }
        return UICollectionReusableView()
    }

    // UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

        collectionView .deselectItemAtIndexPath(indexPath, animated: true)
    }
//    // UICollectionViewDelegateFlowLayout
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//
//        return CGSize(width: UIScreen.mainScreen().bounds.size.width / 7.0, height: 40)
//    }

    // Quickening event
    func quickening(let sender: UIButton) {


    }

}
