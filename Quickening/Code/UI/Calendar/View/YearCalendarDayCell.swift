//
//  YearCalendarDayCell.swift
//  Quickening
//
//  Created by xn011644 on 5/18/16.
//  Copyright © 2016 Jasper. All rights reserved.
//

import UIKit
import SnapKit

class YearCalendarDayCell: BaseCollectionViewCell {

    var dayLabel: UILabel?
    var contentButton: UIButton?
    var topLineView: UIView?

    var selectedContentBackgroundImage: UIImage? {

        didSet {

            if self.selected {

                self.contentButton?.setBackgroundImage(selectedContentBackgroundImage,
                                                       forState: .Disabled)
            }
        }
    }
    var highlightedContentBackgroundImage: UIImage? {

        didSet {

            if self.highlighted {

                self.contentButton?.setBackgroundImage(highlightedContentBackgroundImage,
                                                       forState: .Disabled)
            }
        }
    }
    var normalContentBackgroundImage: UIImage? {

        didSet {

            if !self.selected
                && !self.highlighted {

                self.contentButton?.setBackgroundImage(normalContentBackgroundImage,
                                                       forState: .Disabled)
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.createSubviews()
        self.addConstraintsForSubview()
        self.backgroundColor = UIColor.whiteColor()

        let selectedBackground = UIView()
        selectedBackground.backgroundColor = UIColor.colorFromHexString("dddddd")
        self.selectedBackgroundView = selectedBackground
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews() {

        let topLineView = UIView()
        topLineView.backgroundColor = UIColor.colorFromHexString("cccccc")
        self.contentView.addSubview(topLineView)
        self.topLineView = topLineView

        let dayLabel = UILabel(font: UIFont.systemFontOfSize(12),
                               textColor: UIColor.grayColor())
        dayLabel.textAlignment = .Right
        self.contentView.addSubview(dayLabel)
        self.dayLabel = dayLabel

        let contentButton = UIButton(type: .Custom)
        contentButton.setTitleColor(UIColor.whiteColor(), forState: .Disabled)
        contentButton.titleLabel?.font = UIFont.systemFontOfSize(14)
        contentButton.setBackgroundImage(UIImage(named: "RoundRedBGSmall"), forState: .Disabled)
        contentButton.enabled = false
        self.contentView.addSubview(contentButton)
        self.contentButton = contentButton
    }

    func addConstraintsForSubview() {

        self.topLineView?.snp_makeConstraints { (make) in

            make.top.equalTo(self.contentView.snp_top)
            make.left.equalTo(self.contentView.snp_left)
            make.right.equalTo(self.contentView.snp_right)
            make.height.equalTo(0.5)
        }

        self.dayLabel?.snp_makeConstraints { (make) in

            make.right.equalTo(self.contentView.snp_right).offset(-3)
            make.top.equalTo(self.contentView.snp_top).offset(5)
        }

        self.contentButton?.snp_makeConstraints { (make) in

            make.center.equalTo(self.contentView.snp_center)
            make.width.greaterThanOrEqualTo(30)
            make.height.greaterThanOrEqualTo(30)
        }
    }

    override var selected: Bool {

        didSet {

            self.refreshContentBackground()
        }
    }

    override var highlighted: Bool {

        didSet {

            self.refreshContentBackground()
        }
    }

    func refreshContentBackground() {

        if self.selected {

            // selected, if has selected background set it, otherwise set normal background
            if let selectedBG = self.selectedContentBackgroundImage {

                self.contentButton?.setBackgroundImage(selectedBG, forState: .Normal)
            } else if let normalBG = self.normalContentBackgroundImage {

                self.contentButton?.setBackgroundImage(normalBG, forState: .Normal)
            }
        } else if self.highlighted {

            // highlighted, if has highlighted background set it, otherwise set normal background
            if let highlightedBG = self.highlightedContentBackgroundImage {

                self.contentButton?.setBackgroundImage(highlightedBG, forState: .Normal)
            } else if let normalBG = self.normalContentBackgroundImage {

                self.contentButton?.setBackgroundImage(normalBG, forState: .Normal)
            }
        } else if let normalBG = self.normalContentBackgroundImage {

            // normal,  set normal background
            self.contentButton?.setBackgroundImage(normalBG, forState: .Normal)
        }
    }
//    override func drawRect(rect: CGRect) {
//        super.drawRect(rect)
//
//        let context = UIGraphicsGetCurrentContext()
//        CGContextSetFillColorWithColor(context,
//                                       UIColor(R: 220, G: 61, B: 21, alpha: 1).CGColor)
//        CGContextAddEllipseInRect(context,
//                                  CGRect(x: self.bounds.size.width / 2 - 15,
//                                         y: self.bounds.size.height / 2 - 15,
//                                         width: 30,
//                                         height: 30))
//        CGContextFillPath(context)
//    }
}
