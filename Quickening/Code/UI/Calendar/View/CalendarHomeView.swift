//
//  CalendarMonthView.swift
//  Quickening
//
//  Created by xn011644 on 5/19/16.
//  Copyright © 2016 Jasper. All rights reserved.
//

import UIKit

class CalendarHomeView: BaseView {

    var collectionView: UICollectionView?
    var bottomContentView: UIView?
    var quickeningButton: UIButton?
    var quickeningTimeLabel: UILabel?

    func createSubviews() {

        let collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.itemSize = CGSize(width: UIScreen.mainScreen().bounds.size.width / 7.0,
                                           height: 80.0)
        collectionLayout.headerReferenceSize = CGSize(width: UIScreen.mainScreen().bounds.size.width,
                                                      height: 40)
        collectionLayout.minimumInteritemSpacing = 0

        let collectionView = UICollectionView(frame: CGRectZero,
                                              collectionViewLayout: collectionLayout)
        self.addSubview(collectionView)
        self.collectionView = collectionView

        let bottomContentView = UIView()
        bottomContentView.backgroundColor = UIColor.whiteColor()
//        bottomContentView.
        self.addSubview(bottomContentView)
        self.bottomContentView = bottomContentView

        let quickeningButton = UIButton(type: .Custom)
        quickeningButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        quickeningButton.setTitleColor(UIColor.whiteColor(), forState: .Highlighted)
        quickeningButton.titleLabel?.font = UIFont.systemFontOfSize(14)
//        quickeningButton.
    }

}
