//
//  CalendarMonthHeaderView.swift
//  Quickening
//
//  Created by xn011644 on 5/18/16.
//  Copyright © 2016 Jasper. All rights reserved.
//

import UIKit
import SnapKit

class CalendarMonthHeaderView: UICollectionReusableView {

    var monthLabel: UILabel?
    var monthLabelCenterXConstraint: ConstraintDescriptionEditable?
    var showAtIndex: Int = -1

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.createSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func createSubviews() {

        let monthLabel = UILabel(font: UIFont.systemFontOfSize(14),
                                 textColor: UIColor.darkGrayColor())
        monthLabel.textAlignment = .Center
        self.addSubview(monthLabel)
        self.monthLabel = monthLabel
    }

    func updateMonthLabelToIndex(let index: Int) {

        if index != self.showAtIndex {

            self.showAtIndex = index
            self.monthLabel?.snp_remakeConstraints(closure: { (make) in

                make.centerY.equalTo(self.snp_centerY)
                make.centerX.equalTo(self.snp_centerX).multipliedBy(CGFloat((index * 2 + 1)) / 7.0)
            })
        }
    }
}
