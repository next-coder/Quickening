//
//  BaseCollectionViewController.swift
//  Quickening
//
//  Created by xn011644 on 5/18/16.
//  Copyright © 2016 Jasper. All rights reserved.
//

import UIKit

class BaseCollectionViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var collectionView: UICollectionView?
    var collectionViewLayout: UICollectionViewLayout

    init(let collectionViewLayout: UICollectionViewLayout) {

        self.collectionViewLayout = collectionViewLayout
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {

        self.collectionViewLayout = UICollectionViewFlowLayout()
        super.init(coder: aDecoder)
    }

    override func loadView() {
        super.loadView()

        let collectionView = UICollectionView(frame: self.view.bounds,
                                              collectionViewLayout: self.collectionViewLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        self.collectionView = collectionView
        self.view.addSubview(collectionView)
    }


    // UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {

        return 1
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 0
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        return UICollectionViewCell()
    }

    // UICollectionViewDelegate
}
