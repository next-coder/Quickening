//
//  NSCalendar+Utils.swift
//  Quickening
//
//  Created by xn011644 on 5/18/16.
//  Copyright © 2016 Jasper. All rights reserved.
//

import Foundation

extension NSCalendar {

    func startDateOfMonth(month: UInt, year: UInt) -> NSDate {

        let dateComponents = NSCalendar.currentCalendar().components([ .Year, .Month, .Day ],
                                                                     fromDate: NSDate())
        dateComponents.day = 1
        return NSCalendar.currentCalendar().dateFromComponents(dateComponents)!
    }

    func endDateOfMonth(month: UInt, year: UInt) -> NSDate {

        let dateComponents = NSCalendar.currentCalendar().components([ .Year, .Month, .Day ],
                                                                     fromDate: NSDate())
        dateComponents.month += 1
        dateComponents.day = 0
        return NSCalendar.currentCalendar().dateFromComponents(dateComponents)!
    }

    func allDatesOfMonth(month: UInt, year: UInt) -> [NSDate] {

        let currentCalendar = NSCalendar.currentCalendar()

        let startDateComponents = currentCalendar.components([ .Year, .Month, .Day ],
                                                                     fromDate: NSDate())
        startDateComponents.day = 1

        let endDateComponents = currentCalendar.components([ .Year, .Month, .Day ],
                                                                     fromDate: NSDate())
        endDateComponents.month += 1
        endDateComponents.day = 0

        var allDates = [NSDate]()
        let dateComponnents = startDateComponents
        for day in startDateComponents.day ..< endDateComponents.day {

            dateComponnents.day = day
            allDates.append(currentCalendar.dateFromComponents(dateComponnents)!)
        }

        return allDates
    }
}
