//
//  CalendarDayModel.swift
//  Quickening
//
//  Created by xn011644 on 5/19/16.
//  Copyright © 2016 Jasper. All rights reserved.
//

import Foundation

class CalendarDayModel: BaseModel {

    let date: NSDate?
    let totalQuickeningCount: UInt
    let legalQuickeningCount: UInt
    let estimatedQuickeningCount: UInt

    override init(_ dic: Dictionary<String, AnyObject>) {

        self.date = dic["date"] as? NSDate
        let totalQuickeningCount = dic["total_count"] as? UInt
        if let totalCount = totalQuickeningCount {

            self.totalQuickeningCount = totalCount
        } else {

            self.totalQuickeningCount = 0
        }

        let legalQuickeningCount = dic["legal_count"] as? UInt
        if let legalCount = legalQuickeningCount {

            self.legalQuickeningCount = legalCount
        } else {

            self.legalQuickeningCount = 0
        }

        let estimatedQuickeningCount = dic["estimated_count"] as? UInt
        if let estimatedCount = estimatedQuickeningCount {

            self.estimatedQuickeningCount = estimatedCount
        } else {

            self.estimatedQuickeningCount = 0
        }

        super.init(dic)
    }

    class func modelFromDictionary(dic: [String: AnyObject]) -> CalendarDayModel? {

        if dic.count <= 0 {

            return nil;
        }
        return CalendarDayModel(dic)
    }

    class func modelArrayFromDictionaryArray(modelDicArray: [ [String: AnyObject] ]) -> [CalendarDayModel] {

        var modelArray = [CalendarDayModel]();
        for value in modelDicArray {

            if let model = self.modelFromDictionary(value) {

                modelArray.append(model)
            }
        }
        return modelArray
    }

}
