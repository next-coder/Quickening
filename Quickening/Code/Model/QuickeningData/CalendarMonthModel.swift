//
//  CalendarMonthModel.swift
//  Quickening
//
//  Created by xn011644 on 5/19/16.
//  Copyright © 2016 Jasper. All rights reserved.
//

import Foundation

class CalendarMonthModel: BaseModel {

    // month value, from 0 to 11, 0 for Jan ...
    let month: Int
    // all the days in this month
    var days: [CalendarDayModel]?

    init(month: Int) {

        var tmpMonth = month
        if tmpMonth < 0 || tmpMonth > 12 {

            tmpMonth = 0
        }
        self.month = month
        super.init([String: String]())
    }
}
